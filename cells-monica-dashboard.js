{
  const {
    html,
  } = Polymer;
  /**
    `<cells-monica-dashboard>` Description.

    Example:

    ```html
    <cells-monica-dashboard></cells-monica-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-monica-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsMonicaDashboard extends Polymer.Element {

    static get is() {
      return 'cells-monica-dashboard';
    }

    static get properties() {
      return {
        paso: {
            type: Boolean,
            value: false
        },
        alumnosArry : {
            type : Object,
            value : {},
            notify : true
        },
        alumnos: {
            type: Object,
            notify: true,
            value: {}
      }
    };
  }
  ready() {
          super.ready();
          this.set('alumnosArry', [
          {
            img: '../images/dragon.jpeg',
            name: 'Oscar',
            last: 'Gonzalez',
            address: 'Lomas Estrella',
            hobbies: ['Videojuegos', 'Futbol', 'Dormir']
          },
          {
            img: 'http://placehold.it/150x150/000000/FFFFFF',
            name: 'Luis',
            last: 'Kelly Díaz',
            address: 'Una casa',
            hobbies: 'Programar'
          },
          {
            img: '../images/tresmonos.jpeg',
            name: 'Jesús',
            last: 'Guzmán Vitte',
            address: 'Calle Rancho La Laguna 46B, Colonia Fraccionamiento San Antonio, Cuautitlán Izcalli, Estado de México',
            hobbies: 'Wu-Shu'
          },
          {
            img: '../images/gato-agosto.jpg',
            name: 'Iris',
            last: 'Lizeth',
            address: 'Puerto Yavaros',
            hobbies: ['Ver peliculas y series', 'salir con amigos y familiares']
          },
          {
            img: '../images/migato.jpg',
            name: 'Jhony Fernando',
            last: 'Bartolo Diaz',
            address: 'Tlalpan Centro, Calle Francinso I Madero',
            hobbies: 'Salir de viaje'
          },
          {
            img: '../images/favicon.ico',
            name: 'Silvino',
            last: 'Piza Gaspar',
            address: 'Zumpango, Estado de México',
            hobbies: ['GYM', 'DIBUJAR', 'ESCUCHAR MÚSICA', 'JUGAR VIDEOJUEGOS', 'SALIR AL CINE']
          },
          {
            img: '../images/avatar.jpg',
            name: 'Victor Manuel',
            last: 'Roman Orozco',
            address: 'sur 119 Iztcalco, CDMX',
            hobbies: 'Futbol'
          },
          {
            img: '../images/icon-72x72.png',
            name: 'Ernesto',
            last: 'Mejia Camacho',
            address: 'calzada de la primera #163. tlahuac',
            hobbies: ['tocar la guitarra', 'jugar video juegos', 'hacer deporte']
          },
          {
            img: '../images/leon.jpeg',
            name: 'Luis Enrique',
            last: 'Ruiz Ruiz',
            address: 'Calle Adios #274',
            hobbies: ['Leer', 'jugar video juegos']
          },
          {
            img: '../images/cara.jpg',
            name: 'Ruben',
            last: 'de la Cruz',
            address: 'Calle 4 tetelpan',
            hobbies: ['Series', 'Musica', 'Jugar videjuegos']
          },
          {
            img: '../images/imagen1.jpeg',
            name: 'Diego',
            last: 'Vazquez Alvarez',
            address: 'av. jazmin 32 a',
            hobbies: 'ir al Cine'
          },
          {
            img: '../images/tony.jpg',
            name: 'Tony',
            last: '',
            address: '',
            hobbies: ['Programar', 'Netflix']
          },
          {
            img: '../images/mon.png',
            name: 'Monica',
            last: 'Rivera Valle',
            address: 'Chimalhuacán, Estado de México',
            hobbies: 'Ver series, películas'
          },
          {
            img: '../images/PkVweyp.jpg',
            name: 'Martin',
            last: 'Juarez',
            address: 'Tlaltenco Tlahuac',
            hobbies: ['leer', 'deportes', 'musica']
          },
          {
            img: '../images/favicon.ico',
            name: 'Alberto',
            last: '',
            address: 'Santiago',
            hobbies: ''
          },
          {
            img: '',
            name: 'memo',
            last: 'Ramirez',
            address: 'toledo col juarez',
            hobbies: ''
          }
          ]);
          console.log(this.alumnos);
        }
validarPase(e){
            console.log(e.detail.value);
            if(e.detail.value === true){
                this.set('paso',e.detail.value);
                console.log(e.detail.value);
               
            }else{
                console.log('No paso');
            }
        }
        salirPage(){
            this.paso = false;
        }
        filtrar(string) {
        if (!string) {
          //establece filtro en nulo para deshabilitar el filtrado
          return null;
        } else {
          // devuelve una función de filtro para la cadena de búsqueda actual
          string = string.toLowerCase();
          return function(alumnosArry) {
            var name = alumnosArry.name.toLowerCase();
            var last = alumnosArry.last.toLowerCase();
            return (name.indexOf(string) != -1 ||  last.indexOf(string) != -1);
          };
        }
      }
    static get template() {
      return html `
      <style include="cells-monica-dashboard-styles cells-monica-dashboard-shared-styles"></style>
      <slot></slot>
      
          <p>Welcome to Cells</p>
           <div class="card">
            <div><monhead-element></monhead-element></div>
        </div>
      
        <template is="dom-if" if="[[paso]]">
                <div><p>PASSOO</div> 
            </div>
        </template>

        <template is="dom-if" if="[[!paso]]">
        <iron-ajax 
                auto 
                url="https://api.chucknorris.io/jokes/random" 
                handle-as="json" 
                last-response="{{ajaxResponse}}"> 
        </iron-ajax>

        <div class="card" id="vistaGeneral">
            <div class="circle">M</div><button style="float: right;" on-click="salirPage">Salir</button>
                <div align="center">
                    <h1>Vista Mónica</h1>
                    <img src="../images/mon.png">
                    <p><b>Nombre:</b> Mónica Rivera Valle</p>
                    <p><b>Dirección:</b> Chimalhuacán, Estado de México</p>
                    <p><b>Hobbie:</b> Ver series, películas</p>
                </div>
            </div>

            <div class="card" align="center">
                <b>API Chuck Norris</b><br />
                <img src="{{ajaxResponse.icon_url}}"><br />
                [[ajaxResponse.value]]
                
            </div>
           
            <div class="card" >
            <h2>Lista de alumnos</h2>
           
            <input type="text" placeholder="Filtro" value="{{buscarCadena::input}}">
               
               <template is="dom-repeat" items="{{alumnosArry}}" 
                filter="{{filtrar(buscarCadena)}}">

                    <div align="center">
                        <img src="{{item.img}}"width="80" height="80">
                        <p>Nombre: <span>{{item.name}}</span></p>
                        <p>Apellido: <span>{{item.last}}</span></p> 
                        <p>Dirección: <span>{{item.address}}</span></p> 
                        <p>Hobbies: <span>{{item.hobbies}}</span></p>
                    </div>  
                </template>
        </div>
      
      `;
    }
  }

  customElements.define(CellsMonicaDashboard.is, CellsMonicaDashboard);
}